<?php

namespace Drupal\db_cache_prefix\Cache;

use Drupal\Core\Cache\DatabaseBackend;
use Drupal\Core\Site\Settings;

/**
 * Defines a custom cache implementation.
 *
 * This cache implementation add a prefix to the CID, in order to avoid race
 * conditions where different code bases are using the same database.
 *
 * @ingroup cache
 */
class PrefixedDatabaseBackend extends DatabaseBackend {

  /**
   * {@inheritdoc}
   */
  public function normalizeCid($cid) {
    $prefix = Settings::get('db_cache_prefix');

    if ($prefix == NULL) {
      return parent::normalizeCid($cid);
    }

    $prefixedCid = implode('_', [$prefix, $cid]);

    return parent::normalizeCid($prefixedCid);
  }

}
