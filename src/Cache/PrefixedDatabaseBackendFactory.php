<?php

namespace Drupal\db_cache_prefix\Cache;

use Drupal\Core\Cache\DatabaseBackendFactory;

/**
 * Extend the DatabaseBackendFactory from Core.
 *
 * Return instances of PrefixedDatabaseBackend.
 */
class PrefixedDatabaseBackendFactory extends DatabaseBackendFactory {

  /**
   * {@inheritdoc}
   */
  public function get($bin) {
    $max_rows = $this->getMaxRowsForBin($bin);
    return new PrefixedDatabaseBackend($this->connection, $this->checksumProvider, $bin, $max_rows);
  }

}
