<?php

namespace Drupal\Tests\config_split\Unit;

use Drupal\Core\Cache\CacheTagsChecksumInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Site\Settings;
use Drupal\db_cache_prefix\Cache\PrefixedDatabaseBackend;
use PHPUnit\Framework\TestCase;

/**
 * Tests the normalizeCid() method.
 *
 * @coversDefaultClass \Drupal\db_cache_prefix\Cache\PrefixedDatabaseBackend
 * @group db_cache_prefix
 */
class NormalizeCidTest extends TestCase {

  /**
   * Normalized method (exposed as a ReflectionMethod because it's protected).
   *
   * @var \ReflectionMethod
   */
  private \ReflectionMethod $normalizeCid;

  /**
   * Cache backend that adds a prefix to the cache id.
   *
   * @var \Drupal\db_cache_prefix\Cache\PrefixedDatabaseBackend
   */
  private PrefixedDatabaseBackend $prefixedDatabaseBackend;

  /**
   * Get an accessible method using reflection.
   */
  public function getAccessibleMethod($class_name, $method_name) {
    $class = new \ReflectionClass($class_name);
    $method = $class
      ->getMethod($method_name);
    $method
      ->setAccessible(TRUE);
    return $method;
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $connection = $this
      ->getMockBuilder(Connection::class)
      ->disableOriginalConstructor()
      ->getMock();

    $cacheTagsChecksumInterface = $this
      ->getMockBuilder(CacheTagsChecksumInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->prefixedDatabaseBackend = new PrefixedDatabaseBackend($connection, $cacheTagsChecksumInterface, 'cache');

    $this->normalizeCid = $this
      ->getAccessibleMethod('Drupal\\db_cache_prefix\\Cache\\PrefixedDatabaseBackend', 'normalizeCid');
  }

  /**
   * Test that the normalizeCid() method works as expected.
   *
   * If no prefix is set, the method should return the same value as Core.
   */
  public function testEmptyCachePrefix() {
    new Settings([]);

    $prefixedCid = $this->normalizeCid
      ->invokeArgs($this->prefixedDatabaseBackend, [
        'foo:bar',
      ]);

    $this->assertEquals('foo:bar', $prefixedCid);
  }

  /**
   * Test that the normalizeCid() method works as expected.
   *
   * If a prefix is set, the method should return the prefixed value.
   */
  public function testCachePrefix() {
    new Settings([
      'db_cache_prefix' => 'test',
    ]);

    $prefixedCid = $this->normalizeCid
      ->invokeArgs($this->prefixedDatabaseBackend, [
        'foo:bar',
      ]);

    $this->assertEquals('test_foo:bar', $prefixedCid);
  }

}
