<?php

namespace Drupal\Tests\db_cache_prefix\Kernel;

use Drupal\Core\Extension\ExtensionDiscovery;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ProfileExtensionList;
use Drupal\KernelTests\KernelTestBase;
use org\bovigo\vfs\vfsStream;
use Symfony\Component\Yaml\Yaml;

/**
 * Setup data structures for testing the race condition on module discovery.
 *
 * @group db_cache_prefix
 */
abstract class AbstractRaceConditionTest extends KernelTestBase {

  /**
   * Filesystem structure without the Devel module.
   *
   * @var array
   */
  protected $withoutDevel;

  /**
   * Filesystem structure with the Devel module.
   *
   * @var \array
   */
  protected $withDevel;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->withoutDevel = [
      'core/profiles/standard/standard.info.yml' => [
        'type' => 'profile',
      ],
      'core/modules/user/user.info.yml' => [
        'type' => 'module',
      ],
      'core/modules/system/system.info.yml' => [
        'type' => 'module',
      ],
      'core/themes/seven/seven.info.yml' => [
        'type' => 'theme',
      ],
    ];

    $this->withDevel = [
      'core/profiles/standard/standard.info.yml' => [
        'type' => 'profile',
      ],
      'core/modules/user/user.info.yml' => [
        'type' => 'module',
      ],
      'core/modules/system/system.info.yml' => [
        'type' => 'module',
      ],
      'modules/contrib/devel/devel.info.yml' => [
        'type' => 'module',
      ],
      'core/themes/seven/seven.info.yml' => [
        'type' => 'theme',
      ],
    ];
  }

  /**
   * Return the list of discovered modules.
   *
   * @param array $filesystem
   *   The filesystem's structure.
   * @param string $type
   *   The type of extension to discover.
   *
   * @return array|null
   *   The list of discovered modules.
   *
   * @throws \Exception
   */
  protected function getExtensionList(array $filesystem, string $type): ?array {
    /** @var \Drupal\Core\Cache\CacheBackendInterface $cache */
    $cache = $this->container->get('cache.default');
    /** @var \Drupal\Core\Extension\InfoParserInterface $info_parser */
    $info_parser = $this->container->get('info_parser');
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $module_handler */
    $module_handler = $this->container->get('module_handler');
    /** @var \Drupal\Core\State\StateInterface $state */
    $state = $this->container->get('state');
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->container->get('config.factory');

    // Set up the filesystem.
    $vfs = vfsStream::setup('root', NULL, $filesystem);
    $root = $vfs->url();

    /*
     * ExtensionDiscovery::scan() caches the result of the scan in a static
     * variable. We need to clear that cache before running the test.
     * On real requests this is not an issue because each request has its own
     * set of class instances.
     *
     * @see \Drupal\Core\Extension\ExtensionDiscovery::scan()
     */
    $this->setProtectedProperty(ExtensionDiscovery::class, 'files', []);

    // Set up both the profile and module extension list services to use the
    // virtual filesystem.
    $profileExtensionList = new ProfileExtensionList($root, 'profile', $cache, $info_parser, $module_handler, $state, 'standard');
    $moduleExtensionList = new ModuleExtensionList($root, $type, $cache, $info_parser, $module_handler, $state, $config_factory, $profileExtensionList, 'standard');

    return $moduleExtensionList->getList();
  }

  /**
   * Adds example files to the filesystem structure.
   *
   * @param array $structure
   *   The filesystem's structure.
   *
   * @return array
   *   A populated filesystem.
   */
  protected function populateFilesystemFromStructure(array $structure): array {
    $filesystem = [];
    foreach ($structure as $file => $info) {
      $name = basename($file, '.info.yml');
      $info += [
        'type' => $info['type'],
        'name' => "Name of ($name)",
        'core' => '8.x',
      ];
      $filesystem[$file] = Yaml::dump($info);
    }

    return $filesystem;
  }

  /**
   * Set the value of a protected property.
   *
   * @param object|string $object
   *   The object's class name.
   * @param string $property
   *   The property's name.
   * @param mixed $value
   *   The value to set.
   *
   * @throws \ReflectionException
   */
  protected function setProtectedProperty($object, string $property, $value) {
    $reflection = new \ReflectionClass($object);
    $property = $reflection->getProperty($property);
    $property->setAccessible(TRUE);
    $property->setValue($object, $value);
  }

}
