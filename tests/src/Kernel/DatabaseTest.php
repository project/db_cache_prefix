<?php

namespace Drupal\Tests\db_cache_prefix\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Site\Settings;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Tests the database prefix.
 *
 * @group db_cache_prefix
 */
class DatabaseTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'db_cache_prefix',
  ];

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    $container
      ->register('cache_factory', 'Drupal\db_cache_prefix\Cache\PrefixedDatabaseBackendFactory')
      ->addArgument(new Reference('database'))
      ->addArgument(new Reference('cache_tags.invalidator.checksum'))
      ->addArgument(new Reference('settings'));
  }

  /**
   * Test that cache ids are prefixed.
   */
  public function testInsert() {
    new Settings([
      'db_cache_prefix' => 'test',
    ]);

    /** @var \Drupal\Core\Cache\CacheBackendInterface $cache */
    $cache = $this->container->get('cache.default');

    $cache->set('foo', 'bar');

    /** @var \Drupal\Core\Database\Connection $database */
    $database = $this->container->get('database');

    $entry = $database->select('cache_default', 'c')
      ->fields('c', ['data'])
      ->condition('cid', 'test_foo')
      ->execute();

    $this->assertEquals('bar', $entry->fetchField());
  }

  /**
   * Test that a $cache->deleteAll() deletes prefixed cache ids.
   */
  public function testDelete() {
    new Settings([
      'db_cache_prefix' => 'test',
    ]);

    /** @var \Drupal\Core\Cache\CacheBackendInterface $cache */
    $cache = $this->container->get('cache.default');

    $cache->set('foo', 'bar');

    $cache->deleteAll();

    /** @var \Drupal\Core\Database\Connection $database */
    $database = $this->container->get('database');

    $entry = $database->select('cache_default', 'c')
      ->fields('c', ['data'])
      ->condition('cid', 'test_foo')
      ->execute();

    $this->assertEquals(FALSE, $entry->fetchField());
  }

  /**
   * Test that a $cache->deleteAll() deletes cache ids with different prefixes.
   */
  public function testDeleteMultiplePrefixes() {
    /** @var \Drupal\Core\Cache\CacheBackendInterface $cache */
    $cache = $this->container->get('cache.default');

    new Settings([
      'db_cache_prefix' => 'test',
    ]);

    $cache->set('foo', 'bar');

    new Settings([
      'db_cache_prefix' => 'test2',
    ]);

    $cache->set('foo', 'bar');

    $cache->deleteAll();

    /** @var \Drupal\Core\Database\Connection $database */
    $database = $this->container->get('database');

    $entry = $database->select('cache_default', 'c')
      ->fields('c', ['data'])
      ->condition('cid', 'test_foo')
      ->execute();

    $this->assertEquals(FALSE, $entry->fetchField());

    $entry = $database->select('cache_default', 'c')
      ->fields('c', ['data'])
      ->condition('cid', 'test2_foo')
      ->execute();

    $this->assertEquals(FALSE, $entry->fetchField());
  }

}
