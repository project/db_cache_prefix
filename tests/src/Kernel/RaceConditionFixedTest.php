<?php

namespace Drupal\Tests\db_cache_prefix\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Test that the race condition is fixed when using db_cache_prefix module.
 *
 * @group db_cache_prefix
 */
class RaceConditionFixedTest extends AbstractRaceConditionTest {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'db_cache_prefix',
  ];

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    $container
      ->register('cache_factory', 'Drupal\db_cache_prefix\Cache\PrefixedDatabaseBackendFactory')
      ->addArgument(new Reference('database'))
      ->addArgument(new Reference('cache_tags.invalidator.checksum'))
      ->addArgument(new Reference('settings'));
  }

  /**
   * If the cache prefix is not set the new module is not discovered.
   */
  public function testNoCachePrefix() {
    $extensionsWithoutDevel = $this->getExtensionList($this->populateFilesystemFromStructure($this->withoutDevel), 'module');

    $this->assertArrayHasKey('user', $extensionsWithoutDevel);
    $this->assertArrayHasKey('system', $extensionsWithoutDevel);
    $this->assertArrayHasKey('standard', $extensionsWithoutDevel);
    $this->assertArrayNotHasKey('devel', $extensionsWithoutDevel);

    $extensionsWithDevelPreFlushCache = $this->getExtensionList($this->populateFilesystemFromStructure($this->withDevel), 'module');

    $this->assertArrayNotHasKey('devel', $extensionsWithDevelPreFlushCache);
  }

  /**
   * If the cache prefix is the same the new module is not discovered.
   */
  public function testSameCachePrefix() {
    new Settings([
      'db_cache_prefix' => 'prefix',
    ]);

    $extensionsWithoutDevel = $this->getExtensionList($this->populateFilesystemFromStructure($this->withoutDevel), 'module');

    $this->assertArrayHasKey('user', $extensionsWithoutDevel);
    $this->assertArrayHasKey('system', $extensionsWithoutDevel);
    $this->assertArrayHasKey('standard', $extensionsWithoutDevel);
    $this->assertArrayNotHasKey('devel', $extensionsWithoutDevel);

    new Settings([
      'db_cache_prefix' => 'prefix',
    ]);

    $extensionsWithDevelPreFlushCache = $this->getExtensionList($this->populateFilesystemFromStructure($this->withDevel), 'module');

    $this->assertArrayNotHasKey('devel', $extensionsWithDevelPreFlushCache);
  }

  /**
   * If the cache prefix changes the new module is successfully discovered.
   */
  public function testDifferentCachePrefix() {
    new Settings([
      'db_cache_prefix' => 'prefix1',
    ]);

    $extensionsWithoutDevel = $this->getExtensionList($this->populateFilesystemFromStructure($this->withoutDevel), 'module');

    $this->assertArrayHasKey('user', $extensionsWithoutDevel);
    $this->assertArrayHasKey('system', $extensionsWithoutDevel);
    $this->assertArrayHasKey('standard', $extensionsWithoutDevel);
    $this->assertArrayNotHasKey('devel', $extensionsWithoutDevel);

    new Settings([
      'db_cache_prefix' => 'prefix2',
    ]);

    $extensionsWithDevelPreFlushCache = $this->getExtensionList($this->populateFilesystemFromStructure($this->withDevel), 'module');

    $this->assertArrayHasKey('devel', $extensionsWithDevelPreFlushCache);
  }

}
