<?php

namespace Drupal\Tests\db_cache_prefix\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Test that Core has a race condition on module discovery.
 *
 * New modules are not discovered if the cache has been populated from an
 * old filesystem.
 *
 * @group db_cache_prefix
 */
class RaceConditionTest extends AbstractRaceConditionTest {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    $container
      ->register('cache_factory', 'Drupal\Core\Cache\DatabaseBackendFactory')
      ->addArgument(new Reference('database'))
      ->addArgument(new Reference('cache_tags.invalidator.checksum'))
      ->addArgument(new Reference('settings'));
  }

  /**
   * Test the presence of a race condition.
   *
   * With the standard DatabaseBackendFactory new modules are not discovered
   * until the cache is cleared.
   */
  public function testRaceCondition() {
    $extensionsWithoutDevel = $this->getExtensionList($this->populateFilesystemFromStructure($this->withoutDevel), 'module');

    $this->assertArrayHasKey('user', $extensionsWithoutDevel);
    $this->assertArrayHasKey('system', $extensionsWithoutDevel);
    $this->assertArrayHasKey('standard', $extensionsWithoutDevel);
    $this->assertArrayNotHasKey('devel', $extensionsWithoutDevel);

    $extensionsWithDevelPreFlushCache = $this->getExtensionList($this->populateFilesystemFromStructure($this->withDevel), 'module');

    $this->assertArrayNotHasKey('devel', $extensionsWithDevelPreFlushCache);

    drupal_flush_all_caches();

    $extensionsWithDevelPostFlushCache = $this->getExtensionList($this->populateFilesystemFromStructure($this->withDevel), 'module');

    $this->assertArrayHasKey('devel', $extensionsWithDevelPostFlushCache);
  }

}
