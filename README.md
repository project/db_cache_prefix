# Database cache prefix

## Background

On cloud environments, the database cache is shared between different Drupal
instances. Some instances may have an updated codebase (like during a new
deployment), this means that the cache can contain stale data.

To avoid this, we can set a cache prefix to make sure that the cache is only
visible by the correct Drupal instance.

## How to use it

Install the module like any other module.

Then, in the settings.php file, add the following line:

```php
$settings['db_cache_prefix'] = 'my_prefix';
```

`db_cache_prefix` can be any string, but it makes sense to set it to some value
derived from the actual deployment ID.
